#include "player.hpp"

    void Player::Write(OutputStream& output)
    {
		/* PLANNED METHODS FOR THE COMPRESSED FLOATS
		gsl::span<std::byte> p_x,p_y,p_z,r_x,r_y,r_z,r_w;
		p_x = CompressFloat(position.x);
		p_y = CompressFloat(position.y);
		p_z = CompressFloat(position.z);
		//gsl::span<std::byte> p_xB = gsl::span<std::byte>(p_x);

		*/

		//WRITE POSITION
		output.Write(position.x);
		output.Write(position.y);
		output.Write(position.z);
		//WRITE ROTATION
		output.Write(rotation.x);
		output.Write(rotation.y);
		output.Write(rotation.z);
		output.Write(rotation.w);
		//WRITE NAME
		output.Write(name,128);

       
    };

    void Player::Read(InputStream& input)
    {
		//WRITE ROTATION
		position.x = input.Read<float>();
		position.y = input.Read<float>();
		position.y = input.Read<float>();
		// READ ROTATION
		rotation.x = input.Read<float>();
		rotation.y = input.Read<float>();
		rotation.z = input.Read<float>();
		rotation.w = input.Read<float>();
		// READ NAME
		auto m_name = input.Read(128);

		std::copy(reinterpret_cast<char*>(m_name.data()),
					reinterpret_cast<char*>(m_name.data()) + m_name.size_bytes(),
						name);



    };
    // NOT YET IMPLEMENTED
    gsl::span<std::byte> Player::CompressFloat(float act)
    {
        int actInt;
        if(act<0)
        {
            actInt= int(act*1000-0.5);
        }
        else if(act>0)
        {
            actInt = int(act*1000+0.5);
        }
        gsl::span<std::byte> octets;// = {(std::byte) 1 };
        

        return octets;

        
    };
