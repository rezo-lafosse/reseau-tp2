#pragma once

#include "linking_context.hpp"
#include <game_object.hpp>

NetworkID LinkingContext::createID(GameObject* obj)
{
	if (auto it = m_ObjtoId.find(obj); it != m_ObjtoId.end())
	{
		return it->second;
	}
	m_ObjtoId[obj] = m_nextID;
	m_IdtoObj[m_nextID] = obj;
	m_nextID++;
};

void LinkingContext::Add(GameObject* obj , NetworkID id)
{
    if (auto it = m_ObjtoId.find(obj); it != m_ObjtoId.end())
	{
		return;
	}

    m_ObjtoId[obj] = id;
    m_IdtoObj[id] = obj;
    m_nextID = m_nextID>=id?m_nextID+1:id+1;
	
};

void LinkingContext::Remove(GameObject* obj)
{
    if (auto it = m_ObjtoId.find(obj); it != m_ObjtoId.end())
	{
		m_IdtoObj.erase(it->second);
        m_ObjtoId.erase(it);
	}
    return;
};

void LinkingContext::Remove(NetworkID id)
{
	if (auto it = m_IdtoObj.find(id); it != m_IdtoObj.end())
	{
		m_IdtoObj.erase(it);
        m_ObjtoId.erase(it->second);
	}
    return;
};



std::optional<NetworkID> LinkingContext::IDFromGameObject(GameObject* obj)
{
	if (auto it = m_ObjtoId.find(obj); it != m_ObjtoId.end())
	{
		return it->second;
	}
	return {};
};
std::optional<GameObject*> LinkingContext::GameObjectFromID(NetworkID id)
{
	if (auto it = m_IdtoObj.find(id); it != m_IdtoObj.end())
	{
		return it->second;
	}
	return {};
}
;

LinkingContext* LinkingContext::instance = new LinkingContext();




