#include "class_registry.hpp"

GameObject* ClassRegistry::Create(ClassID id)
{
    if (auto it = m_IdtoGJ.find(id); it != m_IdtoGJ.end())
	{
		return it->second();
	}
    return{};
    
};
ClassRegistry *ClassRegistry::instance = NULL;
    