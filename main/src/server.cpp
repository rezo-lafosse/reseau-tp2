#include <server.hpp>

Server::Server(std::string ip , uint16_t port,uvw::Loop &loop)
{
    std::shared_ptr<uvw::TCPHandle> tcp = loop.resource<uvw::TCPHandle>();
    std::cout << "Listening...";
    tcp->once<uvw::ListenEvent>([](const uvw::ListenEvent &, uvw::TCPHandle &srv) {
        std::shared_ptr<uvw::TCPHandle> client = srv.loop().resource<uvw::TCPHandle>();

        client->on<uvw::CloseEvent>([ptr = srv.shared_from_this()](const uvw::CloseEvent &, uvw::TCPHandle &) { ptr->close(); });
        client->on<uvw::EndEvent>([](const uvw::EndEvent &, uvw::TCPHandle &client) { client.close(); });

        srv.accept(*client);
        clients.push_back(client);
        
        client->read();
    });
};

void Server::Send(uint8_t* ptr , uint32_t size)
{
    std::cout << "Sending data to cliens" << std::endl;

	std::for_each(clients.begin(),clients.end(), [ptr, size](auto actClient)
    {
		actClient->write(reinterpret_cast<char*>(ptr), size); 
	}); 
}