#include "enemy.hpp"


    void Enemy::Write(OutputStream& output)
    {
        //WRITE POSITION
		output.Write(position.x);
		output.Write(position.y);
		output.Write(position.z);
		//WRITE ROTATION
		output.Write(rotation.x);
		output.Write(rotation.y);
		output.Write(rotation.z);
		output.Write(rotation.w);
		//WRITE NAME
		output.Write(type,128);
    };

    void Enemy::Read(InputStream& input)
    {
        //READ ROTATION
		position.x = input.Read<float>();
		position.y = input.Read<float>();
		position.y = input.Read<float>();
		// READ ROTATION
		rotation.x = input.Read<float>();
		rotation.y = input.Read<float>();
		rotation.z = input.Read<float>();
		rotation.w = input.Read<float>();
		// READ NAME
		auto m_name = input.Read(128);

		std::copy(reinterpret_cast<char*>(m_name.data()),
					reinterpret_cast<char*>(m_name.data()) + m_name.size_bytes(),
						type);
    };