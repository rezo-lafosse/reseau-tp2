#include "client.hpp"

Client::Client(std::string ip , uint16_t port,uvw::Loop &loop)
{

    auto tcp = loop.resource<uvw::TCPHandle>();

    tcp->on<uvw::ErrorEvent>([](const uvw::ErrorEvent &, uvw::TCPHandle &) { /* handle errors */ });

    tcp->on<uvw::ConnectEvent>([](const uvw::ConnectEvent &, uvw::TCPHandle &tcp) {
        auto dataWrite = std::unique_ptr<char[]>(new char[2]{ 'b', 'c' });
        tcp.write(std::move(dataWrite), 2);
        tcp.read();
    });

    tcp->on<uvw::DataEvent>([](const uvw::DataEvent& evt, uvw::TCPHandle &){
        std::cout << evt.data.get() << std::endl;
        std::string res(evt.data.get(), evt.length);
        InputStream input(res);
        ReplicationManager* rm = ReplicationManager::Get();
        rm->Replicate(input);

        std::unordered_set<GameObject*> allGOs = rm->getReplicatedObjects();
        for (auto actGO : allGOs)
        {
            actGO->Print();
        }
    });

    tcp->connect(ip, port);

    
};

