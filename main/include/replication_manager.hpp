
#include "player.hpp"
#include "enemy.hpp"
#include "linking_context.hpp"
#include "class_registry.hpp"
#include <unordered_set>

using PacketID = uint64_t;
class ReplicationManager
{
    public :
        void Replicate(MemoryStream m_stream, std::vector<GameObject*> v_gameobjects);
        void Replicate(MemoryStream m_stream);
        void Replicate(OutputStream output ,std::vector<GameObject*> v_gameobjects)
        {
            output.Write(protocolID);
            output.Write(packetID);
            NetworkID actNetworkID;
            ClassRegistry* actRegistry = ClassRegistry::Get();
            ClassID actClassID;
            for(GameObject* act : v_gameobjects)
            {                
                if(auto netID = LinkingContext::Get()->IDFromGameObject(act))
                {
                    NetworkID actNetworkID = netID.value();
                }
                else
                {
                    LinkingContext::Get()->createID(act);
                    NetworkID actNetworkID = LinkingContext::Get()->IDFromGameObject(act).value();
                }
                
                if(typeid(act) == typeid(Player))
                {
                    actClassID= actRegistry->Add<Player>();
                    output.Write(actClassID);
                    output.Write(actNetworkID);
                    ((Player*) act)->Write(output);
                }
                else if(typeid(act) == typeid(Enemy))
                {
                    actClassID= actRegistry->Add<Enemy>();
                    output.Write(actClassID);
                    output.Write(actNetworkID);
                    ((Enemy*) act)->Write(output);
                }
                else
                {
                    actClassID = actRegistry->Add<GameObject>();
                    output.Write(actClassID);
                    output.Write(actNetworkID);
                    act->Write(output);
                }
               

            }
            output.Flush();
            packetID++; 
            
        }
        void Replicate(InputStream input)
        {
            if(input.Read<uint32_t>()!=protocolID)
            {
                return;
            }
            if(input.Read<PacketID>()!=packetID)
            {
                assert(false);
            }
            std::unordered_set<GameObject*> actGOs;
            while(auto idClass = input.Read<ClassID>())
            {
                NetworkID actNetworkID = input.Read<NetworkID>();
                GameObject* actGameObject;
                std::optional<GameObject*> actGameObjectOpt = LinkingContext::Get()->GameObjectFromID(actNetworkID);
                if(!actGameObjectOpt.has_value())
                {
                    actGameObject = ClassRegistry::Get()->Create(idClass);
                    LinkingContext::Get()->Add(actGameObject, actNetworkID);
                }
                else
                {
                    actGameObject = actGameObjectOpt.value();
                }
                if(idClass == Player::mClassID)
                {
                    ((Player*) actGameObject)->Read(input);
                }
                else if(idClass == Enemy::mClassID)
                {
                    ((Enemy*) actGameObject)->Read(input);
                }
                else
                {
                    actGameObject->Read(input);
                }
                actGOs.insert(actGameObject);
            }
            for(GameObject* act : replicatedObjects)
            {
                auto search = actGOs.find(act);
                if (search == actGOs.end())
                {
                    LinkingContext::Get()->Remove(act);
                }
            }
            packetID++;
            replicatedObjects= actGOs;
        }

	static ReplicationManager *Get()
	{
		if(!instance)
		{
			instance = new ReplicationManager();
		}
		return instance;
	}
    
    std::unordered_set<GameObject*> getReplicatedObjects()
    {
        return replicatedObjects;
    }


    private:
        static ReplicationManager* instance;
        static const uint32_t protocolID = 156852524;
        PacketID packetID = 1;
        std::unordered_set<GameObject*> replicatedObjects;
};