#include <cstdint>
#include <iostream>
#include <memory>
#include "uvw.hpp"
#include <replication_manager.hpp>
#include <linking_context.hpp>
#include <class_registry.hpp>


class Client
{
    public :
        Client(std::string ip , uint16_t port,uvw::Loop &loop);
};