#pragma once

#include <map>
#include "game_object.hpp"
#include <functional>
#include <optional>

using NetworkID = uint64_t;

class LinkingContext
{
public :

	NetworkID createID(GameObject* obj);
    void Add(GameObject* obj , NetworkID id);
    void Remove(GameObject* obj);
    void Remove(NetworkID id);
	std::optional<NetworkID> IDFromGameObject(GameObject* obj);
	std::optional<GameObject*> GameObjectFromID(NetworkID id);


	static LinkingContext *Get()
	{
		if(!instance)
		{
			instance = new LinkingContext();
		}
		return instance;
	}


private:

	static LinkingContext* instance;
	std::map<NetworkID, GameObject*> m_IdtoObj;
	std::map<GameObject*, NetworkID> m_ObjtoId;
	NetworkID m_nextID;
};