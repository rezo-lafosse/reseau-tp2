#pragma once
#include <map>
#include "game_object.hpp"
#include <functional>
#include <optional>

using ClassID  = ReplicationClassID;

class ClassRegistry
{
    
    
    public :
        GameObject* Create(ClassID id);
        template<typename T>
        ClassID Add()
        {  
            if (auto it = m_IdtoGJ.find(T::mClassID); it != m_IdtoGJ.end())
            {
                return T::mClassID;
            }
           
            m_IdtoGJ[T::mClassID]= &T::CreateInstance;


            return T::mClassID;
        }

        static ClassRegistry *Get()
        {
            if(!instance)
            {
                instance = new ClassRegistry();
            }
            return instance;
        }
        
    



    private :   
        std::map<ClassID,std::function<GameObject*()>> m_IdtoGJ;
        static ClassRegistry* instance;
};