#pragma once
#include <cstdint>
#include "streams.hpp"

using ReplicationClassID = uint32_t;
struct Rotation_t
{
    float x;
    float y;
    float z;
    float w;
};
struct Position_t
{
    float x;
    float y;
    float z;
};
#define REPLICATED(id,className)                                    \
enum : ReplicationClassID{mClassID = id};                           \
virtual ReplicationClassID ClassID() const  { return mClassID;}     \
static GameObject* CreateInstance() {return new className();}       \


class GameObject
{
public:
    REPLICATED(183355,GameObject);
    virtual void Destroy();
    virtual void Write(OutputStream& output);
    virtual void Read(InputStream& input);
    virtual std::string Print();


};