
#include "game_object.hpp"
#include <cstdint>
#include <iostream>
using namespace std;

class Enemy : GameObject
{
    public : 
        REPLICATED(338535,Enemy);
        void Write(OutputStream& output) override;
        void Read(InputStream& input) override;
        std::string Print() override
        {
            std::cout << "Enemy : " <<  type << std::endl;
            std::cout << "Position (x,y,z) : (" << position.x << " , " << position.y << " , " << position.z << " )" << std::endl;  
            std::cout << "Rotation (x,y,z,w) : (" << rotation.x << " , " << rotation.y << " , " << rotation.z << " , " << rotation.w << " )" << std::endl;  
            std::cout << std::endl;
        }
    private:
        Position_t position;
        char type[128];
        Rotation_t rotation;
};