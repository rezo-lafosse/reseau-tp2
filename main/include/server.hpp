#pragma once
#include <cstdint>
#include "streams.hpp"
#include <iostream>
#include <memory>
#include <uvw.hpp>
#include "client.hpp"
using ClientPtr = std::shared_ptr<uvw::TCPHandle>;
std::vector<ClientPtr> clients;
class Server
{
    public :
        Server(std::string ip , uint16_t port,uvw::Loop &loop );
        void Send(uint8_t* ptr , uint32_t size);
        void AddClient(ClientPtr& client);
};