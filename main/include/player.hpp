#include "game_object.hpp"
#include <gsl/gsl>
#include "streams.hpp"
#include <iostream>
using namespace std;

class Player : GameObject
{
    public :
        REPLICATED(511765,Player);
        void Write(OutputStream& output) override;
        void Read(InputStream& input) override;
        void Position(float x, float y , float z)
        {
            position.x= x;
            position.y = y;
            position.z = z;
        };
        Position_t Position() const { return position;};
        void Rotation(float x, float y , float z , float w)
        {
            rotation.x=x;
            rotation.y=y;
            rotation.z=z;
            rotation.w=w;
        };
        Rotation_t Rotation() const { return rotation;};
        std::string Print() override
        {
            std::cout << "PLAYER : " <<  name << std::endl;
            std::cout << "Position (x,y,z) : (" << position.x << " , " << position.y << " , " << position.z << " )" << std::endl;  
            std::cout << "Rotation (x,y,z,w) : (" << rotation.x << " , " << rotation.y << " , " << rotation.z << " , " << rotation.w << " )" << std::endl;  
            std::cout << std::endl;
        }


    private:
        Position_t position;
        char name[128];
        Rotation_t rotation;
        gsl::span<std::byte> CompressFloat(float act);
        
};