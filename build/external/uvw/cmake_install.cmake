# Install script for directory: C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/tp2")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xuvwx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "C:/Program Files (x86)/tp2/include/uvw/async.hpp;C:/Program Files (x86)/tp2/include/uvw/check.hpp;C:/Program Files (x86)/tp2/include/uvw/dns.hpp;C:/Program Files (x86)/tp2/include/uvw/emitter.hpp;C:/Program Files (x86)/tp2/include/uvw/fs.hpp;C:/Program Files (x86)/tp2/include/uvw/fs_event.hpp;C:/Program Files (x86)/tp2/include/uvw/fs_poll.hpp;C:/Program Files (x86)/tp2/include/uvw/handle.hpp;C:/Program Files (x86)/tp2/include/uvw/idle.hpp;C:/Program Files (x86)/tp2/include/uvw/lib.hpp;C:/Program Files (x86)/tp2/include/uvw/loop.hpp;C:/Program Files (x86)/tp2/include/uvw/pipe.hpp;C:/Program Files (x86)/tp2/include/uvw/poll.hpp;C:/Program Files (x86)/tp2/include/uvw/prepare.hpp;C:/Program Files (x86)/tp2/include/uvw/process.hpp;C:/Program Files (x86)/tp2/include/uvw/request.hpp;C:/Program Files (x86)/tp2/include/uvw/resource.hpp;C:/Program Files (x86)/tp2/include/uvw/signal.hpp;C:/Program Files (x86)/tp2/include/uvw/stream.hpp;C:/Program Files (x86)/tp2/include/uvw/tcp.hpp;C:/Program Files (x86)/tp2/include/uvw/thread.hpp;C:/Program Files (x86)/tp2/include/uvw/timer.hpp;C:/Program Files (x86)/tp2/include/uvw/tty.hpp;C:/Program Files (x86)/tp2/include/uvw/udp.hpp;C:/Program Files (x86)/tp2/include/uvw/underlying_type.hpp;C:/Program Files (x86)/tp2/include/uvw/util.hpp;C:/Program Files (x86)/tp2/include/uvw/work.hpp")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "C:/Program Files (x86)/tp2/include/uvw" TYPE FILE PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ FILES
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/async.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/check.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/dns.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/emitter.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/fs.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/fs_event.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/fs_poll.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/handle.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/idle.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/lib.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/loop.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/pipe.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/poll.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/prepare.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/process.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/request.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/resource.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/signal.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/stream.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/tcp.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/thread.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/timer.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/tty.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/udp.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/underlying_type.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/util.hpp"
    "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw/work.hpp"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xuvwx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "C:/Program Files (x86)/tp2/include/uvw.hpp")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "C:/Program Files (x86)/tp2/include" TYPE FILE PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ FILES "C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/external/uvw/src/uvw.hpp")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("C:/Users/Tabouley/Desktop/UQAC/Rezo/TP2/network_tp2/build/external/uvw/docs/cmake_install.cmake")

endif()

